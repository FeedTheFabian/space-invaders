import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Shield extends Actor
{
    int hp = 5;
    Shield(){
        //Farbe/Bild ändern, eventl. mit Aufblinken
        getImage().scale(80,80);
    }

    public void act(){
        beruerung();
    }

    public void beruerung(){
        
        if(isTouching(Geschoss.class)){
            hp = hp - 1;
            removeTouching(Geschoss.class);
            //Lebensverringerung und Entfernung des Geschosses
        }
        if(hp == 0){
            getWorld().removeObject(this);

            //Zerstörung des Schildes
        }
    }
}
