import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class NeuStarten extends Actor
{
    
    public void act() 
    {
        neuStart();
    }  
    
    public void neuStart(){
    //Neue Startwelt wird erschafen
    if(Greenfoot.isKeyDown("Enter")){
    World start = new Start();
    Greenfoot.setWorld(start);
    }
    }
    
    public void unsichtbarMachen(){
    //macht den Actor unsichtbar
    GreenfootImage image = getImage();
    image.clear();
    }
}
