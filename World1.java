import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class World1 extends World
{
    private Counter scoreCounter;
    private Counter hpCounter;
    World1(){
        
        super(800, 480, 1);
        //Zurücksetzten der statischen Variablen
        MainCharacter.score =0;
        MainCharacter.hp = 0;
        //Festlegen der Counter
        scoreCounter = new Counter("Score: ");
        hpCounter = new Counter("HP: ");
        Greenfoot.setSpeed(50);
        //Festlegen des Hintergrunds
        GreenfootImage background = new GreenfootImage("Kreml.PNG");
        background.scale(800,480);
        setBackground(background);
        //Hinzufügen von Putin
        addMainC();
        //Hinzufügen der Gegner
        addGegener();
        //Schild hinzufügen
        addSchild();
        //Counter hinzufügen
        addObject(scoreCounter,50,15);
        addObject(hpCounter,getWidth()-50,15);
    }
    
    public void act(){
        //Aktualisieren der Counter
        scoreCounter.setValue(MainCharacter.score);
        hpCounter.setValue(MainCharacter.hp);
    }
    
    
    private void platziereObjekte(int min,int max, Class clazz){
        // int k = 0;
        // for (int i = randomNumber(min); i < max; i++) {
            // int x = Greenfoot.getRandomNumber(300);
            // int y = Greenfoot.getRandomNumber(100);
            // if (
            // getObjectsAt(x, y,null).isEmpty() && y > 0 
            // ) {
                // try{addObject((Actor)clazz.newInstance(), x, y);}
                // catch(Exception e){
                    // nix zu tun
                // }
            // }
            // k = k +1;
        // }

    }

    int randomNumber(int n) {
        int zufallszahl;
        zufallszahl = Greenfoot.getRandomNumber(n);
        return zufallszahl;
    }

    void addMainC(){
        MainCharacter mainCharacter = new MainCharacter();
        addObject(mainCharacter,400,460);
    }

    void addSchild(int k,int w){
        Shield schild = new Shield();
        addObject(schild,k,w);
    }
    
    void addGegener(){
    int x = 0;
    int y = 100;
    for(int i = 0;i < 7;i++){
    x = x +110;
    addObject(new Gegener(),x,y);
    
    }

    }
    
    void addSchild(){
        int x = 254;
        int y = 320;
        for(int i = 0; i<2;i++){
            Shield schild = new Shield();
            addObject(schild,x,y);
            x += 294;
        }
    }
}
