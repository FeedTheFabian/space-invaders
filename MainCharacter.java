import greenfoot.*;   // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class MainCharacter extends Actor
{   
    private static final int MOVE = 3;
    static int highscore;
    static String spielerName = Greenfoot.ask("Please enter PlayerName ");
    static int score;
    //Leben von Putin
    static int hp;
    //Multiplikator zum verändern der Geschwindigkeit
    int multiplikator;
    //boolean zum aktivieren der PowerUps
    boolean aktiv = false;
    //timer für die PowerUps
    int counter; 
    public MainCharacter() {
    scaleImage();
    multiplikator = 1;
    hp = 3;
    counter = 0; 
    }
    public void act() 
    {   
        addPutinsSchild();
        schießen();
        bewegen(multiplikator);
        schneller();
        damage();
        powerUps(aktiv,30,1);
        powerUps(aktiv,15,2);
        powerUps(aktiv,20,3);
    } 
    
    public void powerUps(boolean aktivieren,int timer,int x){
    boolean aktiv;
    aktiv= aktivieren;
    //timer für die Länge der PowerUps
    int timerLeft = timer;
    //verschiedene PowerUps werden aktiviert
    if(x == 1 && timerLeft >= 0 && aktiv){
    schneller();
    }
    if(x == 2 && timerLeft >= 0 && aktiv){
    unsterblich();
    }
    if(x == 3 && timerLeft >= 0 && aktiv){
    addPutinsSchild();
    }
    timerLeft--;
    //PowerUp wird deaktiviert
     if(timerLeft <= 0){
    aktiv = false;
    }
    
    }
    
    public void schneller(){
    //Speed Powerup eingesammelt
    //Putin wird schneller
    if(this.isTouching(PowerupSpeed.class)){
    multiplikator = 2;
    aktiv = true;
    }
    
    }
    public void unsterblich(){
    //Unsterblich Power up eingesammeltPowerup eingesammelt
    //Putin wird unsterblich
    if(this.isTouching(PowerupInvinceble.class)){
    hp = 999;
    aktiv = true;
    }
    }
   public void addPutinsSchild(){
       //Schild PowerUp eingesamellt
       //Putin bekommt ein Schild,dass Schüsse blockt
     if(this.isTouching(PutinsSchild.class)){
        getWorld().addObject(new PutinsSchild(),getX() + 10, getY() + 10);
        aktiv= true; 
        }
    }
   
    public void schießen(){
    //Methode zum schießen
    String keyPressed = Greenfoot.getKey();
    if(keyPressed == "space"){
        Geschoss geschoss = new Geschoss();
        getWorld().addObject(geschoss,getX(),getY()-40);
    }
    }

    public void bewegen(int multiplikator){
        //Methode zum Bewegen
        //multiplikator bei Speed Power Up
        if(Greenfoot.isKeyDown("right")){
          move(MOVE*multiplikator);  
        }
        if(Greenfoot.isKeyDown("left")){
        move(-MOVE*multiplikator);
        }
    }
    public void damage(){
    // if(this.isTouching(Geschoss.class)){
    // getImage().setTransparency(20);// Änderung der Durchsichtigkeit / Helligkeit eines Objektes = blinken
    // counter++;
    // if(counter == 10){
    // getImage().setTransparency(200); 
    // counter = 0;
    // }
    // }
    if(hp == 0){
    //Neue Losescreenw welt wird erschaffen
     World lose = new Lose();
     Greenfoot.setWorld(lose);
 
    }
    }
    public void scaleImage(){
    //setzt das Bild auf eine passende Größe    
    getImage().scale(60,80);
    }
    
    public void treffeGeschossGegner(){
     // if(isTouching(GeschossGegner.class)){
            // hp--;
            // getWorld().removeObject(GeschossGegner.class);

        // }
    
    
    }
    
    
}