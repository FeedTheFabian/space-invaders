import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

public class GeschossGegner extends Actor {

  GeschossGegner() {
    scaleImage();
  }

  public void act() {
    schießen();
    schaden();
  }

  public void schießen() {
    move(2);
  }

  public void schaden() {
    List<Actor> actorsInRange = getObjectsInRange(30, null);
    
    if (actorsInRange.size() > 0) {
        Object actor = actorsInRange.get(0);
      if(actor instanceof  MainCharacter){
      MainCharacter.hp--;    
      getWorld().removeObject(this);
    
    }
    }else if(isAtEdge()){
    getWorld().removeObject(this);
    }
    
  }

  public void scaleImage() {
    getImage().scale(5, 10);
    setRotation(90);
  }
}
