import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class GifHintergrund here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GifHintergrund extends Actor
{
    private GifImage g = new GifImage("Hintergrund.gif");
    public void act() 
    {
        getWorld().setBackground(g.getCurrentImage());
    }    
}
