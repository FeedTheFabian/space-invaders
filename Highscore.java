import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Properties;
import java.io.*;

public class Highscore 
{
    private static final String HIGHSCORE = "highscore";
    private static final String FILENAME = "Highscore.properties";
    private static final String PLAYERNAME  = "playerName";
    
    public  static void highScore () throws Exception{
        int score = MainCharacter.score;
        Properties p = new Properties();
        FileReader reader = new FileReader(FILENAME);
        p.load(reader);  

        String highscore = p.getProperty(HIGHSCORE);
        int x  = Integer.parseInt(highscore);

        if(x < score){
            OutputStream os = new FileOutputStream(FILENAME);
            p.setProperty(HIGHSCORE,Integer.toString(score));
            p.setProperty(PLAYERNAME ,MainCharacter.spielerName);
            p.store(os,null);

        }    

    }
    

}

  

