import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Starttext extends Actor {
    //Variable welche für das blinken verwendet wird 
    int i;
    //Text der Angezeigt werden soll
    String text;
    //Neues Bild auf welchem der Text gedruckt wird
    GreenfootImage img = new GreenfootImage(700, 100);
    public Starttext(String text) {
    this.text = text;
    Greenfoot.setSpeed(25);
    }

    public void act() {
        anfangen();
        text();
    }

    public void anfangen() {
        //Starten des spiels
        if (Greenfoot.isKeyDown("Space")) {
            World World1 = new World1();
            Greenfoot.setWorld(World1);
            getWorld().removeObject(this);
        } else {}
    }

    public void text() {
       //Änderung der Schriftfarbe und Größe (Blinken)
       //Änderung der Schriftgröße
       if (i%3 == 0){
       schriftGröße(40);
        }
       if (i%3 != 0){
       schriftGröße(30);
        }
       //Änderung der Schriftfarbe
        if (i%4 == 0){
        schriftFarbe(img,text,Color.GREEN);
       }
       if (i%4 == 1){
       schriftFarbe(img,text,Color.ORANGE);
        }
       if(i%4 == 2){
        schriftFarbe(img,text,Color.YELLOW);
        }
       if(i%4 == 3){
        schriftFarbe(img,text,Color.RED);
        }
       
       i++;
    }

    public void schriftFarbe(GreenfootImage img,String text,Color main){
        //Methode zum ändern der Schriftfarbe 
        img.clear();
        img.setColor(main);
        img.drawString(text, 30,70);
        setImage(img);
    }
    
    public void schriftGröße(int x){
     //Methode zum ändern der Schriftgröße
     //Benutzt Schriftart muss installiert sein
     img.setFont(new Font("Papyrus", false, false , x));
    }
    
}
