import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Start extends World
{
    private GifImage g = new GifImage("Hintergrund.gif");
    public Start()
    {    
        super(800, 480, 1);
        //Hinzufügen der Schrift
        addObject(new Starttext("Fabian, Maksim und Jakob present"),350,30);
        addObject(new Starttext("UdSSR Invaders"),400,350);
        addObject(new Starttext("Press space to start"),400,440);
        addObject(new GifHintergrund(),0,0);
        setBackground(g.getCurrentImage());
        
        //Hinzufügen des Hintergrunds
        //GreenfootImage background = new GreenfootImage("Background Start.jpg");
        //Skalierung des Hintergrunds
        //background.scale(800,480);
        //setBackground(background);
        
        
        
    }
 }