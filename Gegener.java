import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Gegener extends Actor
{
    int hp;
    int counter = Greenfoot.getRandomNumber(1000);
    Gegener(){
        scaleImage();
        hp = 1;
    }

    public void act() 
    {
        dead();
        if(hp > 0){
            move();
        }
        
        if(counter % 100 == 0) {
            schießen();
        }
        counter++;
    }   

    void dead(){
        if(isTouching(Geschoss.class)){
            hp -= hp;  
        }
        if(hp == 0){
            SoeinFeuerball junge = new SoeinFeuerball();
            getWorld().addObject(junge, getX(),getY());
            MainCharacter.score++;
            getWorld().removeObject(this); 
        }
    }

    void schießen(){
        GeschossGegner geschoss = new GeschossGegner();
        getWorld().addObject(geschoss,getX(),getY() +60);
    }

    public void scaleImage(){
        //setzt das Bild auf eine passende Größe  
        getImage().scale(100,80);
        setRotation(90);
    }

    public void move(){
        //Bewegung des Gegners
        setRotation(360);
        move(3);
        setRotation(90);
        //Bei berührung des Randes Gegner auf gegenüberliegende Seite gesetzt
        if(this.isAtEdge()){
            setLocation(50,100);
        }
    }
}
